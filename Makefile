all:
	cargo build --release

clean:
	cargo clean

size: all
	du -b target/wasm32-unknown-unknown/release/cart.wasm

run: all
	w4 run-native target/wasm32-unknown-unknown/release/cart.wasm

bundle: all
	mkdir -p distrib/web
	mkdir -p distrib/linux
	mkdir -p distrib/win

	w4 bundle target/wasm32-unknown-unknown/release/cart.wasm --title "Alpha Orbit" \
	--linux distrib/linux/alpha-orbit \
	--windows distrib/win/alpha-orbit.exe \
	--html distrib/web/index.html
