
use super::*;

#[derive(Copy, Clone)]
pub struct Mine {
    pub x: f32,
    pub y: f32
}

impl Mine {
    pub fn draw(&self) {
        oval(self.x as i32, self.y as i32, 5, 5);
        hline(self.x as i32, self.y as i32 + 2, 5);
        vline(self.x as i32 + 2, self.y as i32, 5);
    }
}

#[derive(Copy, Clone)]
pub struct Projectile {
    pub x: f32,
    pub y: f32,
    pub x_vel: f32,
    pub y_vel: f32
}

impl Projectile {
    pub fn draw(&self) {
        let length = (self.x_vel.powi(2) + self.y_vel.powi(2)).sqrt();

        line(self.x as i32, self.y as i32, (self.x - self.x_vel / length) as i32, (self.y - self.y_vel / length) as i32);
    }

    pub fn update(&mut self) {
        self.x += self.x_vel * DELTA;
        self.y += self.y_vel * DELTA;
    }
}

#[derive(Copy, Clone)]
pub struct Enemy {
    pub x: f32,
    pub y: f32,
    pub x_vel: f32,
    pub y_vel: f32,
    pub offset: f32,
    pub cooldown: f32,
    pub mine_cooldown: f32,
}

impl Enemy {
    pub fn draw(&self) {
        oval(self.x as i32, self.y as i32, 10, 5);
    }

    pub fn update(&mut self, projectiles: &mut [Option<Projectile>], mines: &mut [Option<Mine>], player: &Player, rng: &Rng) {
        // Movement
        if self.x + self.offset < 30.0 && self.x_vel < 0.0 {
            self.y_vel = -30.0;
            self.x_vel = 0.0;
            self.offset = rng.i32(0..20) as f32;
        }

        if self.y < 30.0 - self.offset && self.y_vel < 0.0 {
            self.y_vel = 0.0;
            self.x_vel = 30.0;
            self.offset = rng.i32(0..20) as f32;
        }

        if self.x > 130.0 + self.offset && self.x_vel > 0.0 {
            self.y_vel = 30.0;
            self.x_vel = 0.0;
            self.offset = rng.i32(0..20) as f32;
        }
        
        if self.y > 130.0 + self.offset && self.y_vel > 0.0 {
            self.y_vel = 0.0;
            self.x_vel = -30.0;
            self.offset = rng.i32(0..20) as f32;
        }
        
        self.x += self.x_vel * DELTA;
        self.y += self.y_vel * DELTA;

        // Firing projectiles
        self.cooldown -= DELTA;
        self.mine_cooldown -= DELTA;

        if self.cooldown <= 0.0 {
            for projectile in projectiles.iter_mut() {
                if projectile.is_none() {
                    let dx = player.x - self.x;
                    let dy = player.y - self.y;
                    let length = (dx.powi(2) + dy.powi(2)).sqrt();

                    let unit_x = dx / length;
                    let unit_y = dy / length;

                    *projectile = Some(Projectile {
                        x: self.x,
                        y: self.y,
                        x_vel: unit_x * PROJECTILE_VELOCITY,
                        y_vel: unit_y * PROJECTILE_VELOCITY,
                    });

                    self.cooldown = ENEMY_FIRING_COOLDOWN;

                    tone(500 | (300 << 16), 15, 100, 1);

                    break;
                }
            }
        }

        if self.mine_cooldown <= 0.0 {
            self.mine_cooldown = ENEMY_FIRING_COOLDOWN * 3.0;
            for mine in mines.iter_mut() {
                if mine.is_none() {
                    *mine = Some(Mine {
                        x: self.x,
                        y: self.y,
                    });

                    break;
                }
            }
        }
    }
}

pub struct Player {
    pub x: f32,
    pub y: f32,
    pub x_vel: f32,
    pub y_vel: f32,
    pub rot: f32,
    pub cooldown: f32,
    pub lives: i8,
    pub i_frames: u8,
    pub i_frame_blink_counter: u8,
    pub i_frames_draw: bool,
}

impl Player {
    pub fn update(&mut self, player_projectiles: &mut [Option<Projectile>]) {
        if self.i_frames > 0 {
            self.i_frames -= 1;
            self.i_frame_blink_counter -= 1;

            if self.i_frame_blink_counter == 0 {
                self.i_frames_draw = !self.i_frames_draw;
                self.i_frame_blink_counter = 10;
            }
        }

        if self.cooldown >= 0. {
            self.cooldown -= DELTA;
        }

        // Controls
        let gamepad = unsafe { *GAMEPAD1 };

        if gamepad & BUTTON_RIGHT != 0 {
            self.rot += 0.1;
        } else if gamepad & BUTTON_LEFT != 0 {
            self.rot -= 0.1;
        }

        if gamepad & BUTTON_UP != 0 {
            self.x_vel += self.rot.sin();
            self.y_vel += -self.rot.cos();
        } else if gamepad & BUTTON_DOWN != 0 {
        }

        if gamepad & BUTTON_1 != 0 && self.cooldown <= 0.0 {
            for projectile in player_projectiles.iter_mut() {
                if projectile.is_none() {
                    let x_vel = self.rot.sin() * PROJECTILE_VELOCITY;
                    let y_vel = -self.rot.cos() * PROJECTILE_VELOCITY;

                    *projectile = Some(Projectile {
                        x: self.x,
                        y: self.y,
                        x_vel,
                        y_vel,
                    });

                    self.cooldown = PLAYER_FIRING_COOLDOWN;

                    tone(700 | (262 << 16), 15, 100, 0);

                    break;
                }
            }
        }

        // Level collisions
        // TODO: Clean up and remove redundant code
        self.x += self.x_vel * DELTA;

        if self.x < 0.0 || self.x > 160.0 {
            self.x_vel = -self.x_vel;
            self.x += self.x_vel * DELTA;

            self.x_vel /= 2.0;
        } else if self.x >= 40.0 && self.x <= 120.0 && self.y >= 40.0 && self.y <= 120.0 {
            self.x_vel = -self.x_vel;
            self.x += self.x_vel * DELTA;

            self.x_vel /= 2.0;
        }

        self.y += self.y_vel * DELTA;

        if self.y < 0.0 || self.y > 160.0 {
            self.y_vel = -self.y_vel;
            self.y += self.y_vel * DELTA;

            self.y_vel /= 2.0;
        } else if self.x >= 40.0 && self.x <= 120.0 && self.y >= 40.0 && self.y <= 120.0 {
            self.y_vel = -self.y_vel;
            self.y += self.y_vel * DELTA;

            self.y_vel /= 2.0;
        }

    }

    pub fn draw(&self) {

        if self.i_frames > 0 && !self.i_frames_draw {
            return;
        }
        
        let x = 2.0;
        let y = 3.0;
        let x1 = x * self.rot.cos() - y * self.rot.sin();
        let y1 = x * self.rot.sin() + y * self.rot.cos();
        
        line(self.x as i32 + x1 as i32, self.y as i32 + y1 as i32, self.x as i32, self.y as i32);

        let x = -2.0;
        let y = 3.0;
        let x1 = x * self.rot.cos() - y * self.rot.sin();
        let y1 = x * self.rot.sin() + y * self.rot.cos();

        line(self.x as i32 + x1 as i32, self.y as i32 + y1 as i32, self.x as i32, self.y as i32);
    }
}

#[derive(Copy, Clone)]
pub struct Particle {
    pub x: f32,
    pub y: f32,
    pub x_vel: f32,
    pub y_vel: f32,
    pub lifetime: f32,
    pub color: u16,
}

impl Particle {
    pub fn draw(&self) {
        unsafe { *DRAW_COLORS = self.color }
        rect(self.x as i32, self.y as i32, 1, 1);
    }

    pub fn update(&mut self) {
        self.lifetime -= DELTA; 

        self.x += self.x_vel;
        self.y += self.y_vel;
    }
}
