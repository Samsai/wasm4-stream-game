
use super::*;

pub struct Music {
    timer: u32,
    index: usize,
    notes: &'static [u32],
}

static WIN_MUSIC: [u32; 3] = [784, 880, 988];
static LOSS_MUSIC: [u32; 3] = [466, 415, 370];

pub struct GameState {
    pub player: Player,
    pub enemies: [Option<Enemy>; MAX_ENEMIES],
    pub player_projectiles: [Option<Projectile>; MAX_PLAYER_PROJECTILES],
    pub enemy_projectiles: [Option<Projectile>; MAX_ENEMY_PROJECTILES],
    pub mines: [Option<Mine>; MAX_MINES],
    pub particles: [Option<Particle>; MAX_PARTICLES],
    pub score: u16,
    pub level: u8,
    pub paused: bool,
    pub rng: Lazy<Rng>,
    pub music: Option<Music>,
}

impl GameState {
    pub fn initialize(&mut self) {
        if self.level > 20 {
            self.paused = false;
            return;
        }

        self.player = Player {
            x: 10.0,
            y: 10.0,
            rot: 0.0,
            x_vel: 0.0,
            y_vel: 0.0,
            cooldown: 0.0,
            lives: if self.player.lives < 3 { self.player.lives + 1 } else { 3 },
            i_frames: 0,
            i_frame_blink_counter: 0,
            i_frames_draw: false,
        };

        self.paused = true;

        for i in 0..self.level {
            self.enemies[i as usize] = Some(Enemy {
                x: self.rng.i32(10..150) as f32,
                y: self.rng.i32(130..150) as f32,
                x_vel: -30.0,
                y_vel: 0.0,
                offset: self.rng.i32(0..20) as f32,
                cooldown: self.rng.f32() * 5.0,
                mine_cooldown: self.rng.f32() * 10.0,
            })
        }

        self.mines = [None; MAX_MINES];
        self.player_projectiles = [None; MAX_PLAYER_PROJECTILES];
        self.enemy_projectiles = [None; MAX_ENEMY_PROJECTILES];
    }

    pub fn tick(&mut self) {
        let mut stop_music = false;

        if let Some(music) = &mut self.music {
            if music.timer == 0 {
                tone(music.notes[music.index], MUSIC_DELAY, 100, 2);

                music.index += 1;

                if music.index == music.notes.len() {
                    stop_music = true;
                }

                music.timer = MUSIC_DELAY;
            } else {
                music.timer -= 1;
            }
        }

        if stop_music {
            self.music = None;
        }

        if self.level > 20 {
            return;
        }

        if self.paused {
            // Controls
            let gamepad = unsafe { *GAMEPAD1 };

            if gamepad & (BUTTON_1 | BUTTON_2) != 0 {
                self.paused = false;
            }

            return;
        }

        let enemy_count = self.enemies.iter().filter(|e| e.is_some()).count();

        if enemy_count == 0 {
            self.level += 1;
            self.music = Some(Music {
                timer: 0,
                index: 0,
                notes: &WIN_MUSIC
            });
            self.initialize();
        }

        if self.player.lives >= 0 {
            self.player.update(&mut self.player_projectiles);
        }

        for enemy in &mut self.enemies {
            if let Some(enemy) = enemy {
                enemy.update(&mut self.enemy_projectiles, &mut self.mines, &self.player, &self.rng);
            }
        }

        for particle in &mut self.particles {
            if let Some(particle) = particle {
                particle.update();
            }
            if particle.is_some() && particle.unwrap().lifetime <= 0.0 {
                *particle = None;
            }
        }

        Self::move_and_destroy_projectiles(&mut self.player_projectiles);

        Self::move_and_destroy_projectiles(&mut self.enemy_projectiles);

        // Player projectile collisions
        for projectile_index in 0..self.player_projectiles.len() {
            for enemy_index in 0..self.enemies.len() {
                if let (Some(projectile), Some(enemy)) = (self.player_projectiles[projectile_index], self.enemies[enemy_index]) {
                    if projectile.x >= enemy.x && projectile.x <= enemy.x + 10.0 && projectile.y >= enemy.y && projectile.y <= enemy.y + 5.0 {
                        self.player_projectiles[projectile_index] = None;
                        self.enemies[enemy_index] = None;
                        self.score += 10;

                        for i in 0..6 {
                            GameState::spawn_particle(&self.rng, &mut self.particles, projectile.x, projectile.y, 4);
                            tone(300 | (50 << 16), 20, 75, 3);
                        }
                    }
                }
            }

            for mine_index in 0..self.mines.len() {
                if let (Some(projectile), Some(mine)) = (self.player_projectiles[projectile_index], self.mines[mine_index]) {
                    if projectile.x >= mine.x && projectile.x <= mine.x + 10.0 && projectile.y >= mine.y && projectile.y <= mine.y + 5.0 {
                        self.player_projectiles[projectile_index] = None;
                        self.mines[mine_index] = None;

                        for i in 0..6 {
                            GameState::spawn_particle(&self.rng, &mut self.particles, projectile.x, projectile.y, 4);
                            tone(300 | (50 << 16), 20, 75, 3);
                        }
                    }
                }
            }
        }

        // Enemy projectile collisions
        for projectile_index in 0..self.enemy_projectiles.len() {
            if let Some(projectile) = self.enemy_projectiles[projectile_index] {
                if self.player.lives < 0 {
                    break;
                }
                if projectile.x >= self.player.x && projectile.x <= self.player.x + 5.0 && projectile.y >= self.player.y && projectile.y <= self.player.y + 5.0 {
                    if self.player.i_frames > 0 {
                        break;
                    }
                    self.enemy_projectiles[projectile_index] = None;
                    self.player.lives -= 1;
                    self.player.i_frames = 100;
                    self.player.i_frame_blink_counter = 10;

                    tone(700 | (50 << 16), 20, 75, 3);

                    if self.player.lives < 0 {
                        self.music = Some(Music {
                            timer: 0,
                            index: 0,
                            notes: &LOSS_MUSIC
                        });
                    }
                }
            }
        }

        // Mine collisions
        for mine_index in 0..self.mines.len() {
            if let Some(mine) = self.mines[mine_index] {
                if self.player.lives < 0 {
                    break;
                }
                if (mine.x + 5.0) >= self.player.x && (mine.x + 5.0) <= self.player.x + 5.0 && (mine.y + 5.0) >= self.player.y && (mine.y + 5.0) <= self.player.y + 5.0 {
                    if self.player.i_frames > 0 {
                        break;
                    }
                    self.mines[mine_index] = None;
                    self.player.lives -= 1;
                    self.player.i_frames = 100;
                    self.player.i_frame_blink_counter = 10;

                    tone(700 | (50 << 16), 20, 75, 3);

                    if self.player.lives < 0 {
                        self.music = Some(Music {
                            timer: 0,
                            index: 0,
                            notes: &LOSS_MUSIC
                        });
                    }
                }
            }
        }
    }

    fn move_and_destroy_projectiles(projectiles: &mut [Option<Projectile>]) {
        for projectile in projectiles {
            let mut should_be_destroyed = false;

            if let Some(projectile) = projectile {
                projectile.update();
        
                if projectile.x < 0.0 || projectile.x > 160.0 || projectile.y < 0.0 || projectile.y > 160.0 {
                    should_be_destroyed = true;
                } else if projectile.x >= 40.0 && projectile.x <= 120.0 && projectile.y >= 40.0 && projectile.y <= 120.0 {
                    should_be_destroyed = true;
                }
            }

            if should_be_destroyed {
                *projectile = None;
            }
        }
    }

    fn spawn_particle(rng: &Rng, particles: &mut [Option<Particle>], x: f32, y: f32, color: u16) {
        for particle in particles {
            if particle.is_none() {
                *particle = Some(Particle {
                    x,
                    y,
                    x_vel: rng.f32() * 0.5,
                    y_vel: rng.f32() * 0.5,
                    lifetime: 0.5,
                    color,
                });

                return;
            }
        }
    }

    pub fn draw(&self) {
        // Level + HUD
        unsafe { *DRAW_COLORS = 0x40 }
        rect(40, 40, 80, 80);

        unsafe { *DRAW_COLORS = 4 }

        if self.paused {
            text("PAUSED", 55, 45);
        }

        let mut buf = [0u8; 64];

        text(
            format_no_std::show(
                &mut buf,
                format_args!("SC: {}", self.score)
            ).unwrap()
            , 45, 80);

        if self.level <= 20 {
            text(
                format_no_std::show(
                    &mut buf,
                    format_args!("LV: {}", self.level)).unwrap(),
                45, 95);
        }

        if self.level > 20 {
            text("YOU WON", 50, 45);
        }
        if self.player.lives >= 0 {
            text("HP:", 45, 65);

            let mut lives_x_offset: f32 = 80.0;

            for i in 0..self.player.lives {
                let life_indicator = Player { x: lives_x_offset, y: 65.0, x_vel: 0.0, y_vel: 0.0, rot: 0.0, cooldown: 0.0, lives: 0, i_frames: 0, i_frame_blink_counter: 0, i_frames_draw: false };
                life_indicator.draw();
                lives_x_offset += 10.0;
            }

            unsafe { *DRAW_COLORS = 2 }
            self.player.draw();
        } else {
            text("GAME OVER", 45, 65);
        }

        unsafe { *DRAW_COLORS = 3 }

        for enemy in &self.enemies {
            if let Some(enemy) = enemy {
                enemy.draw();
            }
        }

        for mine in &self.mines {
            if let Some(mine) = mine {
                mine.draw();
            }
        }

        unsafe { *DRAW_COLORS = 2 }
        
        for projectile in &self.player_projectiles {
            if let Some(projectile) = projectile {
                projectile.draw();
            }
        }

        for projectile in &self.enemy_projectiles {
            if let Some(projectile) = projectile {
                projectile.draw();
            }
        }

        for particle in &self.particles {
            if let Some(particle) = particle {
                particle.draw();
            }
        }
    }
}
