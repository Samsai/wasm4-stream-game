#![no_std]

// #[cfg(feature = "buddy-alloc")]
// mod alloc;
mod wasm4;
use wasm4::*;

use fastrand::Rng;

use once_cell::sync::Lazy;

const DELTA: f32 = 1.0/60.0;
const PROJECTILE_VELOCITY: f32 = 100.0;
const MAX_ENEMIES: usize = 20;
const MAX_ENEMY_PROJECTILES: usize = 3;
const MAX_PLAYER_PROJECTILES: usize = 4;
const MAX_MINES: usize = 30;
const MAX_PARTICLES: usize = 30;
const ENEMY_FIRING_COOLDOWN: f32 = 3.0;
const PLAYER_FIRING_COOLDOWN: f32 = 0.5;

const MUSIC_DELAY: u32 = 30;

mod gamestate;
use gamestate::*;
mod entities;
use entities::*;

static mut game: GameState = GameState {
    player: Player { x: 10.0, y: 10.0, rot: 0.0, x_vel: 0.0, y_vel: 0.0, cooldown: 0.0, lives: 3, i_frames: 0, i_frame_blink_counter: 0, i_frames_draw: false },
    player_projectiles: [None; 4],
    enemy_projectiles: [None; MAX_ENEMY_PROJECTILES],
    enemies: [None; MAX_ENEMIES],
    mines: [None; MAX_MINES],
    particles: [None; MAX_PARTICLES],
    score: 0,
    level: 1,
    paused: true,
    rng: Lazy::new(|| Rng::with_seed(4)),
    music: None
};
    
#[no_mangle]
fn start() {
    unsafe {
        *PALETTE = [
            0x000000,
            0x00aa00,
            0xaa0000,
            0xaaaaaa,
        ];
    }

    unsafe {
        game.initialize();
    }
}

#[no_mangle]
fn update() {

    unsafe {
        game.tick();
        game.draw();
    }
}
